import React, {useState, useEffect} from 'react'
import Sidebar from '../../components/Admin/sidebar'
import Login from './Routes/Login'
import { useHistory} from 'react-router-dom'

const Admin = () => {
    const idUser = localStorage.getItem('userId')
    const history = useHistory()

    return(
        <>
            {idUser ? <Sidebar history={history} /> : <Login />}
        </>
    )
}

export default Admin