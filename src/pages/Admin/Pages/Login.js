import React, {useState} from 'react'
import { LoginFormContainer } from '../../../styles'
import { useHistory} from 'react-router-dom'
import api from '../../../services/api'

const Login = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const history = useHistory()

    const handleLogin = async (e) => {
        e.preventDefault()

        try{
            const response = await api.post('/login', { username, password })
            console.log('RESPONSEEE',response)
            localStorage.setItem('userId', response.data.id)
            //localStorage.setItem('ongName', response.data.name)
            history.push('/admin')
        }catch(err){
            alert('Falha no login, usuário ou senha incorretos.')
        }
    }

    return(
        <LoginFormContainer>
            <form onSubmit={handleLogin}>
                    <h1>Faça seu Login</h1>
                    <div className="form-group">
                        <input placeholder="Usuário" value={username} onChange={ e => setUsername( e.target.value ) } />
                    </div>
                    <div className="form-group">
                        <input placeholder="Senha" type="password" value={password} onChange={ e => setPassword( e.target.value ) } />
                    </div>
                    <button className="button" type="submit">
                        Entrar
                    </button>
                </form>
        </LoginFormContainer>
    )
}

export default Login