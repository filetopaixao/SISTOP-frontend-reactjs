import React, {useEffect, useState} from 'react'
import DataTable from 'react-data-table-component';
import Modal from '../../../components/Admin/Modal'
import ReactDOM from 'react-dom';
//forms
import FormNoticias from '../../../components/Admin/Noticias/FormNoticias'
import FormNoticiasEdit from '../../../components/Admin/Noticias/FormNoticiasEdit'
import FormProducoes from '../../../components/Admin/Producoes/FormProducoes'
import FormProducoesEdit from '../../../components/Admin/Producoes/FormProducoesEdit'
import FormUsuarios from '../../../components/Admin/Usuarios/FormUsuarios'
import FormUsuariosEdit from '../../../components/Admin/Usuarios/FormUsuariosEdit'

import api from '../../../services/api'
var dateFormat = require('dateformat');



export default class TableList extends React.Component{
    state = {
      title: ''
    }


    handleCadastro = () => {
        if(this.props.selected == 'noticias'){
            ReactDOM.render(
                <FormNoticias />,
                document.getElementById('modal-body')
              );
        }
    }


    clicked = async (e) => {
      console.log(e.id)
      if(e.id){
        if(this.props.selected == 'noticias'){
          var title, subtitle, author, date, image, content = ''
          window.$('#exampleModal').modal('show');
          await api.get('/noticias')
            .then(async res => {
              res.data.noticias.map( noticia => {
                  if(noticia.id == e.id){
                    console.log('Achou a noticia', e.id)
                    title = noticia.title
                    subtitle = noticia.subtitle
                    author = noticia.author
                    date = dateFormat(noticia.news_date, "yyyy-mm-dd")
                    image = noticia.image
                    content = noticia.content_text
                  }
                })
            })
          ReactDOM.render(
            <FormNoticiasEdit id={e.id} title={title} subtitle={subtitle} author={author} date={date} image={image} content={content} />,
            document.getElementById('modal-body')
          );

        }else if(this.props.selected == 'producao'){
          var title, category, year, link, description = ''
          window.$('#exampleModal').modal('show');
          await api.get('/producoes')
            .then(async res => {
              res.data.producoes.map( producao => {
                  if(producao.id == e.id){
                    title = producao.title
                    category = producao.category
                    year = producao.year
                    link = producao.link
                    description = producao.description
                  }
                })
            })
          ReactDOM.render(
            <FormProducoesEdit id={e.id} title={title} category={category} year={year} link={link} image={image} description={description} />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'user'){
          var name, username, password, role = ''
          window.$('#exampleModal').modal('show');
          await api.get('/usuarios')
            .then(async res => {
              res.data.usuarios.map( user => {
                  if(user.id == e.id){
                    name = user.name
                    username = user.username
                    password = user.password
                    role = user.role
                  }
                })
            })
          ReactDOM.render(
            <FormUsuariosEdit id={e.id} name={name} username={username} password={password} role={role} />,
            document.getElementById('modal-body')
          );
        }
      }else{
        window.$('#exampleModal').modal('show');
        if(this.props.selected == 'noticias'){
          ReactDOM.render(
            <FormNoticias />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'producao'){
          ReactDOM.render(
            <FormProducoes />,
            document.getElementById('modal-body')
          );
        }else if(this.props.selected == 'user'){
          ReactDOM.render(
            <FormUsuarios />,
            document.getElementById('modal-body')
          );
        }
      }
    }


    render() {
        if(this.props.selected == 'noticias'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Título',
              selector: 'title',
              sortable: true,
            },
            ,
            {
              name: 'Autor',
              selector: 'author',
              sortable: true,
            },
            ,
            {
              name: 'Data de Publicação',
              selector: 'published_date',
              sortable: true,
              format: row => dateFormat(row.news_date, "dd/mm/yyyy")
            },
          ];
        }else if(this.props.selected == 'producao'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Título',
              selector: 'title',
              sortable: true,
            },
            ,
            {
              name: 'Categoria',
              selector: 'category',
              sortable: true,
            },
            ,
            {
              name: 'Data de Publicação',
              selector: 'published_date',
              sortable: true,
              format: row => dateFormat(row.news_date, "dd/mm/yyyy")
            },
          ];
        }else if(this.props.selected == 'user'){
          var columns = [
            {
              name: 'Id',
              selector: 'id',
              sortable: true,
              width: '50px'
            },
            {
              name: 'Nome',
              selector: 'name',
              sortable: true,
            },
            {
              name: 'Usuário',
              selector: 'username',
              sortable: true,
            },
            {
              name: 'Função',
              selector: 'role',
              sortable: true,
            },
          ];
        }
        

        if(this.props.selected == 'home'){
            return (<h1>As</h1>)
        }else{
             return(
                <>
                <div style={{ display: 'flex', justifyContent: 'space-between'}}>
                  <div>
                      <h1>{this.props.title}</h1>
                  </div>
                  <div>
                    <button type="button" class="btn btn-primary" onClick={this.clicked}>
                        Cadastrar
                    </button>
                  </div>
                </div>
                
                <DataTable
                    noHeader={true}
                    columns={columns}
                    data={this.props.data}
                    Clicked
                    onRowClicked={this.clicked}
                    pointerOnHover
                    pagination
                    paginationPerPage={5}
                />

                <Modal selected={this.props.selected} title={this.state.title} />
                
                </>
             )
        }

      }
}