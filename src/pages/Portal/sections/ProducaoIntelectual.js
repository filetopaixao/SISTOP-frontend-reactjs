import React, {useState, useEffect} from 'react'
import {Section} from '../../../styles'
import Navtabs from '../../../components/Portal/ProducaoIntelectual/NavTabs'
import api from '../../../services/api'

const ProducaoIntelectual = () => {

    const [producaoCientifica, setProducaoCientifica] = useState([])
    const [producaoTecnica, setProducaoTecnica] = useState([])
    const [softwares, setSoftwares] = useState([])

      useEffect(async () =>{
        const res = await api.get('/producoes')
        var pc = []
        var pt = []
        var s = []
        await res.data['producoes'].map((producao)=>{
            if(producao.category == 'cientifica'){
                pc = [...pc, producao]
                
            }else if(producao.category == 'tecnica'){
                pt = [...pt, producao]
            }else if(producao.category == 'software'){
                s = [...s, producao]
            }

            setProducaoCientifica( pc )
            setProducaoTecnica(pt)
            setSoftwares(s)
        })
      }, [])

    
    return(
        <div style={{ backgroundColor:'#fff'}}>
            <Section className="container" id="producao-intelectual">
                <h2>Produção Intelectual</h2>
                <Navtabs producaoCientifica={producaoCientifica} producaoTecnica={producaoTecnica} softwares={softwares} />
            </Section>
        </div>
    )
}

export default ProducaoIntelectual