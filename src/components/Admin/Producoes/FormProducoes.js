import React, {useState, useMemo } from 'react'
import api from '../../../services/api'

const FormProducoes = (props) => {
    const [title, setTitle ] = useState('')
    const [category, setCategory ] = useState('')
    const [year, setYear ] = useState('')
    const [link, setLink ] = useState('')
    const [description, setDescription ] = useState('')

    const handleProducao = async (e) => {
        e.preventDefault()

        const idUser = localStorage.getItem('userId')

        var data = {
            title,
            category,
            year,
            link,
            description
        }

        try{
            await api.post('/producoes', data, {
                headers: {
                    Authorization: idUser
                }
            })

            setTitle('')
            setCategory('')
            setYear('')
            setLink('')
            setDescription('')
            
            alert('Produção intelectual cadastrada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Produção intelectual, tente novamente.')
        }
    }


    return(
        <form onSubmit={handleProducao}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Título</label>
                <input type="text" className="form-control" placeholder="Título da Produção Intelectual" value={title} onChange={ e => setTitle(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Descrição</label>
                <textarea class="form-control" placeholder="Descrição da Produção Intelectual" rows="3" value={description} onChange={ e => setDescription(e.target.value)}></textarea>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Categoria</label>
                <select class="form-control" id="exampleFormControlSelect1" value={category} onChange={ e => setCategory(e.target.value)}>
                    <option value="cientifica">Produção Científica</option>
                    <option value="tecnica">Produção Técnica</option>
                    <option value="software">Software</option>
                </select>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Ano</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Ano da Produção Intelectual" value={year} onChange={ e => setYear(e.target.value)} />
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Link</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Lnik da Produção Intelectual" value={link} onChange={ e => setLink(e.target.value)} />
            </div>
                
            
            <button type="submit" className="btn btn-primary">Cadastrar</button>
        </form>
    )
}

export default FormProducoes