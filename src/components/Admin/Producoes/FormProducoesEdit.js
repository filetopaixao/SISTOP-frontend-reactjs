import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
var createObjectURL = require('create-object-url');

const FormProducoesEdit = (props) => {

    const [title, setTitle ] = useState(props.title)
    const [category, setCategory ] = useState(props.category)
    const [year, setYear ] = useState(props.year)
    const [link, setLink ] = useState(props.link)
    const [description, setDescription ] = useState(props.description)

    const handleProducao = async (e) => {
        e.preventDefault()

        var data = {
            title,
            category,
            year,
            link,
            description
        }
       

        try{
            await api.put('/producoes/' + props.id, data)
            
            alert('Produção Intelectual atualizada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Produção Intelectual, tente novamente.')
        }
    }

    const deleteProducao = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/producoes/' + props.id)
                
                alert('Produção Intelectual deletada com sucesso!')
            }catch(err){
                alert('Erro ao deletar a Produção Intelectual, tente novamente.')
            }
            //this.props.history.push('/admin')
        }
        //props.id
        
        
    }



    return(
        <form onSubmit={handleProducao}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Título</label>
                <input type="text" className="form-control" placeholder="Título da Produção Intelectual" value={title} onChange={ e => setTitle(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Descrição</label>
                <textarea class="form-control" placeholder="Descrição da Produção Intelectual" rows="3" value={description} onChange={ e => setDescription(e.target.value)}></textarea>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Categoria</label>
                <select class="form-control" id="exampleFormControlSelect1" value={category} onChange={ e => setCategory(e.target.value)}>
                    <option value="cientifica">Produção Científica</option>
                    <option value="tecnica">Produção Técnica</option>
                    <option value="software">Software</option>
                </select>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Ano</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Ano da Produção Intelectual" value={year} onChange={ e => setYear(e.target.value)} />
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Link</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Lnik da Produção Intelectual" value={link} onChange={ e => setLink(e.target.value)} />
            </div>
                
            
            <button type="submit" className="btn btn-primary" style={{marginRight: '15px'}}>Atualizar</button>
            <button onClick={deleteProducao} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormProducoesEdit