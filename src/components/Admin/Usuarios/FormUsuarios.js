import React, {useState, useMemo } from 'react'
import api from '../../../services/api'

const FormUsuarios = (props) => {
    const [name, setName ] = useState('')
    const [username, setUsername ] = useState('')
    const [password, setPassword ] = useState('')
    const [role, setRole ] = useState('editor')

    const handleUsuario = async (e) => {
        e.preventDefault()

        const idUser = localStorage.getItem('userId')

        var data = {
            name,
            username,
            password,
            role
        }

        try{
            await api.post('/usuarios', data, {
                headers: {
                    Authorization: idUser
                }
            })

            setName('')
            setUsername('')
            setPassword('')
            setRole('')
            
            alert('Produção intelectual cadastrada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Produção intelectual, tente novamente.')
        }
    }


    return(
        <form onSubmit={handleUsuario}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Nome</label>
                <input type="text" className="form-control" placeholder="Nome do Usuário" value={name} onChange={ e => setName(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Usuário</label>
                <input type="text" className="form-control" placeholder="Login do Usuário" value={username} onChange={ e => setUsername(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Senha</label>
                <input type="text" className="form-control" type="password" placeholder="Senha do Usuário" value={password} onChange={ e => setPassword(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Função</label>
                <select class="form-control" id="exampleFormControlSelect1" value={role} onChange={ e => setRole(e.target.value)}>
                    <option value="editor">Editor</option>
                    <option value="admin">Administrador</option>
                </select>
            </div>
                
            
            <button type="submit" className="btn btn-primary">Cadastrar</button>
        </form>
    )
}

export default FormUsuarios