import React, {useState, useMemo } from 'react'
import api from '../../../services/api'

const FormUsuariosEdit = (props) => {
    const [name, setName ] = useState(props.name)
    const [username, setUsername ] = useState(props.username)
    const [password, setPassword ] = useState(props.password)
    const [role, setRole ] = useState(props.role)

    const handleUsuario = async (e) => {
        e.preventDefault()

        const idUser = localStorage.getItem('userId')

        var data = {
            name,
            username,
            password,
            role
        }

        try{
            await api.put('/usuarios/'+ props.id, data)

            setName('')
            setUsername('')
            setPassword('')
            setRole('')
            
            alert('Produção intelectual cadastrada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Produção intelectual, tente novamente.')
        }
    }

    const deleteUsuario = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/usuarios/' + props.id)
                
                alert('Usuario deletado com sucesso!')
            }catch(err){
                alert('Erro ao deletar o Usuario, tente novamente.')
            }
            //this.props.history.push('/admin')
        }
        //props.id
        
        
    }


    return(
        <form onSubmit={handleUsuario}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Nome</label>
                <input type="text" className="form-control" placeholder="Nome do Usuário" value={name} onChange={ e => setName(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Usuário</label>
                <input type="text" className="form-control" placeholder="Login do Usuário" value={username} onChange={ e => setUsername(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Senha</label>
                <input type="text" className="form-control" type="password" placeholder="Senha do Usuário" value={password} onChange={ e => setPassword(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Função</label>
                <select class="form-control" id="exampleFormControlSelect1" value={role} onChange={ e => setRole(e.target.value)}>
                    <option value="editor">Editor</option>
                    <option value="admin">Administrador</option>
                </select>
            </div>
                
            
            <button type="submit" className="btn btn-primary" style={{marginRight: '15px'}}>Atualizar</button>
            <button onClick={deleteUsuario} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormUsuariosEdit