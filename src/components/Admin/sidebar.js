import React from 'react'
import SideNav, {  NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import styled from 'styled-components';
import TableList from '../../pages/Admin/Routes/TableList'
import api from '../../services/api'
import { FaHome, FaNewspaper, FaSignOutAlt , FaLightbulb, FaUserAlt } from "react-icons/fa";


const Main = styled.main`
    position: relative;
    overflow: hidden;
    transition: all .15s;
    padding: 0 20px;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
`;

export default class Sidebar extends React.Component{
    state = {
        selected: localStorage.getItem('selected'),
        expanded: false,
        data: {},
        title: ''
    };

    handleLogout = () => {
        const r = window.confirm("Do you really want to Sign Out?");
        if(r == true){
            localStorage.clear()
            this.props.history.push('/admin')
        }else{

        }
    }

    async componentDidMount(){
        if(this.state.selected == 'noticias'){
            this.setState({data:{} , title: 'Notícias'})
             await api.get('/noticias')
             .then(async res => {
                 await this.setState({data: res.data['noticias']})
             })
        }else if(this.state.selected == 'producao'){
             this.setState({data:{}, title: 'Produção Intelectual'})
             await api.get('/producoes')
             .then(async res => {
                 await this.setState({data: res.data['producoes']})
             })
        }else if(this.state.selected == 'user'){
            this.setState({data:{}, title: 'Usuários'})
            await api.get('/usuarios')
            .then(async res => {
                await this.setState({data: res.data['usuarios']})
            })
       }
    }

    onSelect =  async (selected) => {
        if(selected == 'logout'){
            localStorage.setItem('selected', 'home')
            await this.setState({ selected: 'home' });
        }else{
            localStorage.setItem('selected', selected)
            await this.setState({ selected: selected });
        }

       if(selected == 'noticias'){
           this.setState({data:{} , title: 'Notícias'})
            await api.get('/noticias')
            .then(async res => {
                await this.setState({data: res.data['noticias']})
            })
       }else if(selected == 'producao'){
            this.setState({data:{}, title: 'Produção Intelectual'})
            await api.get('/producoes')
            .then(async res => {
                await this.setState({data: res.data['producoes']})
            })
       }else if(selected == 'user'){
            this.setState({data:{}, title: 'Usuários'})
            await api.get('/usuarios')
            .then(async res => {
                await this.setState({data: res.data['usuarios']})
            })
       }
    };

    onToggle = (expanded) => {
        this.setState({ expanded: expanded });
    };

    renderBreadcrumbs() {
        const { selected } = this.state;
        var key = ''
    
        return (
            <>
            <div>
                <TableList selected={selected} data={this.state.data} title={this.state.title}/>
            </div>
            </>
        );
    }

    navigate = (pathname) => () => {
        this.setState({ selected: pathname });
    };

    render() {
        const { expanded, selected } = this.state;

        return (
            <div>
                <div
                    style={{
                        marginLeft: expanded ? 240 : 64,
                        padding: '15px 20px 0 20px'
                    }}
                >
                
                    
                </div>
                <SideNav onSelect={this.onSelect} onToggle={this.onToggle}>
                    <SideNav.Toggle />
                    <SideNav.Nav selected={selected}>
                        <NavItem eventKey="home">
                            <NavIcon>
                                <FaHome className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Home">
                                Home
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="user">
                            <NavIcon>
                                <FaUserAlt className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Usuários">
                                Usuários
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="noticias">
                            <NavIcon>
                                <FaNewspaper className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Noticias">
                                Notícias
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="producao">
                            <NavIcon>
                                <FaLightbulb className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Produção Científica">
                                Produção Intelectual
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="logout" onClick={this.handleLogout}>
                            <NavIcon>
                                <FaSignOutAlt className='social-icon' size="20" color="#fff" />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Reports">
                                Sair
                            </NavText>
                        </NavItem>
                        <NavItem eventKey="settings">
                            <NavIcon>
                                <i className="fa fa-fw fa-cogs" style={{ fontSize: '1.5em', verticalAlign: 'middle' }} />
                            </NavIcon>
                            <NavText style={{ paddingRight: 32 }} title="Settings">
                                Settings
                            </NavText>
                            <NavItem eventKey="settings/policies">
                                <NavText title="Policies">
                                    Policies
                                </NavText>
                            </NavItem>
                            <NavItem eventKey="settings/network">
                                <NavText title="Network">
                                    Network
                                </NavText>
                            </NavItem>
                        </NavItem>
                    </SideNav.Nav>
                </SideNav>
                <Main expanded={expanded}>
                    {this.renderBreadcrumbs()}
                </Main>
            </div>
        );
    }
}