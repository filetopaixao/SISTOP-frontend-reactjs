import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
//import MultiUpload from './MultiUpload'
var createObjectURL = require('create-object-url');

const FormNoticias = (props) => {
    var fileObj = [];
    var fileArray = [];
    const idUser = localStorage.getItem('userId')


    const [title, setTitle ] = useState('')
    const [subtitle, setSubtitle ] = useState('')
    const [author, setAuthor ] = useState('')
    const [news_date, setNewsDate ] = useState('')
    const [image, setImage ] = useState(null)
    const [content_text, setContentText ] = useState('')
    const [file, setFile] = useState([])

    const preview = useMemo(() => {
        return image ? createObjectURL(image) : null;
      }, [image])

      const uploadMultipleFiles = (e) => {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setFile( fileArray )
    }

    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleNews = async (e) => {
        e.preventDefault()

        
        const data = new FormData();
        const data2 = new FormData();

        data.append('image', image);
        data2.append('gallery', file);
        data.append('title', title);
        data.append('subtitle', subtitle);
        data.append('author', author);
        data.append('news_date', news_date);
        data.append('content_text', content_text);
         
        /*
        const data = {
            title,
            subtitle,
            author,
            news_date,
            published_date: datePublished,
            image: image,
            content_text
        }
       */

        console.log(data2)
        try{
            await api.post('/noticias', data, {
                headers: {
                    Authorization: idUser
                }
            })

            setTitle('')
            setSubtitle('')
            setAuthor('')
            setNewsDate('')
            setImage('')
            setContentText('')
            
            alert('Notícia cadastrada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Noticia, tente novamente.')
        }
        /*
        try{
            await api.post('/noticias/uploads', data2)
            
            alert('Notícia cadastrada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Noticia, tente novamente.')
        }
        */
    }


    return(
        <form onSubmit={handleNews}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Título</label>
                <input type="text" className="form-control" placeholder="Título da Notícia" value={title} onChange={ e => setTitle(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Subtítulo</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Breve descrição da Notícia" value={subtitle} onChange={ e => setSubtitle(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Autor</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Autor da Notícia" value={author} onChange={ e => setAuthor(e.target.value)} />
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Data da Notícia</label>
                <input type="date" className="form-control" id="formGroupExampleInput2" placeholder="Data da Notícia"  value={news_date} onChange={ e => setNewsDate(e.target.value)}/>
            </div>

            <div className="form-group">
                <div><label for="formGroupExampleInput2">Imagem Destaque</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '200px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>

            <div className="form-group">
                <label for="formGroupExampleInput2">Conteúdo</label>
                <textarea class="form-control" placeholder="Conteúdo da Notícia" rows="3" value={content_text} onChange={ e => setContentText(e.target.value)}></textarea>
            </div>
            
            {/*
            <div className="form-group multi-preview">
                    {(fileArray || []).map(url => (
                        <img src={url} alt="..." />
                    ))}
                </div>
               }
                <div className="form-group">
                    <input type="file" className="form-control" onChange={uploadMultipleFiles} multiple />
                    <button type="button" className="btn btn-danger btn-block" onClick={uploadFiles}>Upload</button>
                </div>
                */}
                
            
            <button type="submit" className="btn btn-primary">Cadastrar</button>
        </form>
    )
}

export default FormNoticias