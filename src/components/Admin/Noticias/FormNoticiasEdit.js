import React, {useState, useMemo } from 'react'
import api from '../../../services/api'
var createObjectURL = require('create-object-url');

const FormNoticiasEdit = (props) => {
    var fileObj = [];
    var fileArray = [];
    const idUser = localStorage.getItem('userId')

    const [title, setTitle ] = useState(props.title)
    const [subtitle, setSubtitle ] = useState(props.subtitle)
    const [author, setAuthor ] = useState(props.author)
    const [news_date, setNewsDate ] = useState(props.date)
    const [image, setImage ] = useState(props.image)
    const [content_text, setContentText ] = useState(props.content)
    const [file, setFile] = useState([])

    const preview = useMemo(() => {
        return typeof image != 'string' ? createObjectURL(image) : image;
      }, [image])

      const uploadMultipleFiles = (e) => {
        fileObj.push(e.target.files)
        for (let i = 0; i < fileObj[0].length; i++) {
            fileArray.push(URL.createObjectURL(fileObj[0][i]))
        }
        setFile( fileArray )
    }

    const uploadFiles = (e) => {
        e.preventDefault()
        console.log(file)
    }

    const handleNews = async (e) => {
        e.preventDefault()

        if(typeof image == 'string' ){
            var data = {
                title,
                subtitle,
                author,
                news_date,
                image: image,
                content_text
            }
        }else{
            var data = new FormData();

            data.append('image', image);
            //data2.append('gallery', file);
            data.append('title', title);
            data.append('subtitle', subtitle);
            data.append('author', author);
            data.append('news_date', news_date);
            data.append('content_text', content_text);

            console.log('DATAAAA', data)
        }
         
        /*
        const data = {
            title,
            subtitle,
            author,
            news_date,
            published_date: datePublished,
            image: image,
            content_text
        }
       */

        try{
            await api.put('/noticias/' + props.id, data)
            
            alert('Notícia atualizada com sucesso!')
        }catch(err){
            alert('Erro ao cadastrar Noticia, tente novamente.')
        }
    }

    const deleteNew = async (e) => {
        e.preventDefault()
        const r = window.confirm("Do you really want to delete it?");
        if(r == true){
            try{
                await api.delete('/noticias/' + props.id)
                
                alert('Notícia deletada com sucesso!')
            }catch(err){
                alert('Erro ao deletar a Notícia, tente novamente.')
            }
            //this.props.history.push('/admin')
        }
        //props.id
        
        
    }



    return(
        <form onSubmit={handleNews}>
            <div className="form-group">
                <label for="formGroupExampleInput2">Título</label>
                <input type="text" className="form-control" placeholder="Título da Notícia" value={title} onChange={ e => setTitle(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Subtítulo</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Breve descrição da Notícia" value={subtitle} onChange={ e => setSubtitle(e.target.value)}/>
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Autor</label>
                <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Autor da Notícia" value={author} onChange={ e => setAuthor(e.target.value)} />
            </div>
            <div className="form-group">
                <label for="formGroupExampleInput2">Data da Notícia</label>
                <input type="date" className="form-control" id="formGroupExampleInput2" placeholder="Data da Notícia"  value={news_date} onChange={ e => setNewsDate(e.target.value)}/>
            </div>

            <div className="form-group">
                <div><label for="formGroupExampleInput2">Imagem Destaque</label></div>
                <label 
                    id="thumbnail" 
                    style={{ backgroundImage: `url(${preview})`, width: '100%', height: '200px', backgroundSize: 'contain', backgroundRepeat: 'no-repeat' }}
                    className={image ? 'has-thumbnail' : ''}
                >
                    
                </label><input type="file" onChange={event => setImage(event.target.files[0])} name="gallery"/>
            </div>

            <div className="form-group">
                <label for="formGroupExampleInput2">Conteúdo</label>
                <textarea class="form-control" placeholder="Conteúdo da Notícia" rows="3" value={content_text} onChange={ e => setContentText(e.target.value)}></textarea>
            </div>

            {/*
            <div className="form-group multi-preview">
                    {(fileArray || []).map(url => (
                        <img src={url} alt="..." />
                    ))}
                </div>

                <div className="form-group">
                    <input type="file" className="form-control" onChange={uploadMultipleFiles} multiple />
                    <button type="button" className="btn btn-danger btn-block" onClick={uploadFiles}>Upload</button>
                </div>
                    */}
                
            
            <button type="submit" className="btn btn-primary" style={{marginRight: '15px'}}>Atualizar</button>
            <button onClick={deleteNew} className="btn btn-danger">Deletar</button>
        </form>
    )
}

export default FormNoticiasEdit