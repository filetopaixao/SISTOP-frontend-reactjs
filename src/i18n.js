import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from './assets/i18n/translations/en.json'
import pt_BR from './assets/i18n/translations/pt_BR.json'

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      pt_BR:{
        translation: pt_BR
      },
      en: {
        translation: en
      }
    },
    lng: "pt_BR",
    fallbackLng: "pt_BR",

    interpolation: {
      escapeValue: false
    }
  });


export default i18n;